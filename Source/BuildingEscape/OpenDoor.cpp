// Fill out your copyright notice in the Description page of Project Settings.

#include "OpenDoor.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	ClosedYaw = GetOwner()->GetActorRotation().Yaw;
	OpenAngle = ClosedYaw + OpenAngle;

	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("Actor &s have no PressurePlate!"), *GetOwner()->GetName());
	}

	ActorThatOpen = GetWorld()->GetFirstPlayerController()->GetPawn();
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(PressurePlate && PressurePlate->IsOverlappingActor(ActorThatOpen))
	{
		OpenDoor(DeltaTime);
		DoorLastOpen = GetWorld()->GetTimeSeconds();
	}

	if (PressurePlate && !PressurePlate->IsOverlappingActor(ActorThatOpen) &&
		GetWorld()->GetTimeSeconds() > (DoorLastOpen + DoorCloseDelay))
	{
		CloseDoor(DeltaTime);
	}
	
}

void UOpenDoor::OpenDoor(float DeltaTime)
{
	FRotator CurrentRotation = GetOwner()->GetActorRotation();
	float CurrentYaw = CurrentRotation.Yaw;
	
	CurrentYaw = FMath::FInterpTo(CurrentYaw, OpenAngle, DeltaTime, OpenSpeed);
	GetOwner()->SetActorRotation(FRotator(CurrentRotation.Pitch, CurrentYaw, CurrentRotation.Roll));
}

void UOpenDoor::CloseDoor(float DeltaTime)
{
	FRotator CurrentRotation = GetOwner()->GetActorRotation();
	float CurrentYaw = CurrentRotation.Yaw;
	
	CurrentYaw = FMath::FInterpTo(CurrentYaw, ClosedYaw, DeltaTime, OpenSpeed);
	GetOwner()->SetActorRotation(FRotator(CurrentRotation.Pitch, CurrentYaw, CurrentRotation.Roll));
}

